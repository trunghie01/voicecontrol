package com.example.voicecontrol;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.os.PowerManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import edu.cmu.pocketsphinx.*;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import static android.widget.Toast.makeText;

public class MyAccessibilityService extends AccessibilityService implements
        RecognitionListener {

    /* Named searches allow to quickly reconfigure the decoder */
    private static final String KWS_SEARCH = "wakeup";
    private static final String DIGITS_SEARCH = "digits";
    private static final String MENU_SEARCH = "menu";

    private static final String SWIPE_SEARCH = "swipe from";
    private static final String HOLD_SEARCH = "hold";
    private static final String CLICK_SEARCH = "click";

    /* Keyword we are looking for to activate menu */
    private static final String KEYPHRASE = "listen to me";

    private SpeechRecognizer recognizer;

    WindowManager windowManager = null;
    PowerManager powerManager = null;
    DisplayMetrics displayMetrics = null;
    WindowManager.LayoutParams glassViewParams = null;
    LinearLayout glassView = null;
    int colNum = 10;
    int rowNum = 20;

    WindowManager.LayoutParams screenOnParams = null;
    LinearLayout screenOnView = null;

    String currentCommand = "";
    String[] digits = {"4","5","7","B","C","H","K","L","M","Q","S","T"};
    HashMap<String, Integer> digitMap = new HashMap<>();

    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    public void onCreate() {
        // Check if user has given permission to record audio
        /*int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO); // TODO get micro and overlay permission
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, 1);
            return;
        }*/

        try {
            Assets assets = new Assets(this);
            File assetDir = assets.syncAssets();
            this.setupRecognizer(assetDir);
            switchSearch(KWS_SEARCH);
        } catch (IOException e) {
            Log.d("z", e.getMessage());
        }

        this.windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        this.powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.displayMetrics = this.getResources().getDisplayMetrics();

        LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        glassView    = (LinearLayout) mInflater.inflate(R.layout.activity_glass, (ViewGroup) null);
        screenOnView = (LinearLayout) mInflater.inflate(R.layout.activity_screen_on, (ViewGroup) null);

        for(int i = 0; i < digits.length; i++){
            digitMap.put(digits[i], i);
        }

        int colWidth = displayMetrics.widthPixels / colNum;
        int colHeight = displayMetrics.heightPixels / rowNum;
        int digitsLen = digits.length;
        for(int i = 0; i < rowNum; i++){
            LinearLayout row = new LinearLayout(this);
            for(int j = 0; j < colNum; j++){
                TextView t = new TextView(this);
                String id = digits[i % digitsLen] + "" + digits[j % digitsLen] + "" + digits[i / digitsLen];
                t.setText(id);
                t.setWidth(colWidth);
                t.setHeight(colHeight);
                t.setGravity(Gravity.CENTER);
                t.setShadowLayer(10, 0, 0, Color.WHITE);
                t.setTypeface(Typeface.DEFAULT_BOLD);
                t.setTextColor(Color.BLACK);
                row.addView(t);
            }
            glassView.addView(row);
        }

        glassViewParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT
        );
        screenOnParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                PixelFormat.TRANSLUCENT
        );
    }


    public void onAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
    }

    public void onServiceConnected() {
        Log.d("z", "connected");
    }

    public void onInterrupt() {
        Log.d("z", "interrupt");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("z", "destroy");

        if (recognizer != null) {
            recognizer.cancel();
            recognizer.shutdown();
        }
    }

    public void setupRecognizer(File assetsDir) throws IOException {
        recognizer = SpeechRecognizerSetup.defaultSetup()
                .setAcousticModel(new File(assetsDir, "en-us-ptm"))
                .setDictionary(new File(assetsDir, "cmudict-en-us.dict"))
                .getRecognizer();
        recognizer.addListener(this);

        recognizer.addKeyphraseSearch(KWS_SEARCH, KEYPHRASE);

        // Create grammar-based search for selection between demos
        File menuGrammar = new File(assetsDir, "menu.gram");
        recognizer.addGrammarSearch(MENU_SEARCH, menuGrammar);

        // Create grammar-based search for digit recognition
        File digitsGrammar = new File(assetsDir, "digits.gram");
        recognizer.addGrammarSearch(DIGITS_SEARCH, digitsGrammar);

        File swipeGrammar = new File(assetsDir, "swipe.gram");
        recognizer.addGrammarSearch(SWIPE_SEARCH, swipeGrammar);

        File holdGrammar = new File(assetsDir, "hold.gram");
        recognizer.addGrammarSearch(HOLD_SEARCH, holdGrammar);

        File clickGrammar = new File(assetsDir, "click.gram");
        recognizer.addGrammarSearch(CLICK_SEARCH, clickGrammar);
    }

    @Override
    public void onBeginningOfSpeech() {
    }

    @Override
    public void onEndOfSpeech() {
        if (!recognizer.getSearchName().equals(KWS_SEARCH))
            switchSearch(KWS_SEARCH);
    }

    private void switchSearch(String searchName) {
        recognizer.stop();

        if (searchName.equals(KWS_SEARCH))
            recognizer.startListening(searchName);
        else
            recognizer.startListening(searchName, 10000);
    }

    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;

        String text = hypothesis.getHypstr();
        switch (text) {
            case KEYPHRASE:
                switchSearch(MENU_SEARCH);
                if(!powerManager.isInteractive()) {
                    screenOn();
                }
                break;
            case SWIPE_SEARCH:
                switchSearch(SWIPE_SEARCH);
                currentCommand = SWIPE_SEARCH;
                windowManager.addView(glassView, glassViewParams);
                break;
            case HOLD_SEARCH:
                switchSearch(HOLD_SEARCH);
                currentCommand = HOLD_SEARCH;
                windowManager.addView(glassView, glassViewParams);
                break;
            case CLICK_SEARCH:
                switchSearch(CLICK_SEARCH);
                currentCommand = CLICK_SEARCH;
                windowManager.addView(glassView, glassViewParams); // TODO still stay when listen time out
                break;
        }
    }

    @Override
    public void onResult(Hypothesis hypothesis) {
//        ((TextView) findViewById(R.id.result_text)).setText("");
        if (hypothesis != null) {
            String text = hypothesis.getHypstr();
            Log.d("z", text);
            if(powerManager.isInteractive()) {
                switch (text) {
                    case "swipe right":
                        touch(0, (float) displayMetrics.heightPixels / 2, displayMetrics.widthPixels, (float) displayMetrics.heightPixels / 2, 1000L);
                        break;
                    case "swipe left":
                        touch(displayMetrics.widthPixels - 10, (float) displayMetrics.heightPixels / 2, 0, (float) displayMetrics.heightPixels / 2, 1000L);
                        break;
                    case "swipe up":
                        touch((float) displayMetrics.widthPixels / 2, displayMetrics.heightPixels - 10, (float) displayMetrics.widthPixels / 2, 0, 1000L);
                        break;
                    case "swipe down":
                        touch((float) displayMetrics.widthPixels / 2, 0, (float) displayMetrics.widthPixels / 2, displayMetrics.heightPixels - 10, 1000L);
                        break;
                    case "click center":
                        touch((float) displayMetrics.widthPixels / 2, (float) displayMetrics.heightPixels / 2, (float) displayMetrics.widthPixels / 2, (float) displayMetrics.heightPixels / 2, 10L);
                        break;
                    case "go back":
                        this.performGlobalAction(GLOBAL_ACTION_BACK);
                        break;
                    case "go home":
                        this.performGlobalAction(GLOBAL_ACTION_HOME);
                        break;
                    case "go recent apps":
                        this.performGlobalAction(GLOBAL_ACTION_RECENTS);
                        break;
                    case "keep screen on":
                        toggleScreenOn(true);
                        break;
                    case "stop keep screen on":
                        toggleScreenOn(false);
                        break;
                }

                switch (currentCommand) {
                    case SWIPE_SEARCH:
                        if (!text.equals(SWIPE_SEARCH)) {
                            int[] pos = processSwipeCommand(text);
                            if (pos[0] != -1 && pos[1] != -1 && pos[2] != -1 && pos[3] != -1) {
                                touch(pos[0], pos[1], pos[2], pos[3], pos[4] == -1 ? 1000L : pos[4]);
                            }
                            if (glassView.getParent() != null) windowManager.removeView(glassView);
                            currentCommand = "";
                        }
                        break;
                    case HOLD_SEARCH:
                        if (!text.equals(HOLD_SEARCH)) {
                            int[] pos = processHoldCommand(text);
                            if (pos[0] != -1 && pos[1] != -1) {
                                touch(pos[0], pos[1], pos[0], pos[1], pos[2] == -1 ? 1000L : pos[2]);
                            }
                            if (glassView.getParent() != null) windowManager.removeView(glassView);
                            currentCommand = "";
                        }
                        break;
                    case CLICK_SEARCH:
                        if (!text.equals(CLICK_SEARCH)) {
                            int[] pos = getPosition(text);
                            if (pos[0] != -1 && pos[1] != -1) {
                                touch(pos[0], pos[1], pos[0], pos[1], 10L);
                            }
                            if (glassView.getParent() != null) windowManager.removeView(glassView);
                            currentCommand = "";
                        }
                        break;
                }

                makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            }
        }
    }

    int[] getPosition(String text){
        if(text.equals("cancel")) return new int[]{1, -1};
        String[] pos = text.toUpperCase().split(" ");
        if(pos.length != 3) return new int[] {-1, -1};
        try {
            int colWidth = displayMetrics.widthPixels / colNum;
            int touchX = colWidth * (digitMap.get(pos[1])) + colWidth / 2;
            int rowHeight = displayMetrics.heightPixels / rowNum;
            int touchY = rowHeight * (digitMap.get(pos[0])) + rowHeight / 2 + getStatusBarHeight(this) + rowHeight * digits.length * digitMap.get(pos[2]);
            return new int[] {touchX, touchY};
        } catch (NullPointerException e){
            Log.d("z", e.getMessage());
            return new int[] {-1, -1};
        }
    }

    int getTime(String time){
        String number = time.replace("second", "").trim();
        if(number.equals("a tenth of")) return 100;
        if(number.equals("a quarter of")) return 250;
        if(number.equals("half")) return 100;
        if(number.equals("")) return -1;
        return  Integer.parseInt(number) * 1000;
    }

    int[] processSwipeCommand(String text){ // TODO bug swipe to not exist coordinate
        if(text.equals("cancel")) return new int[]{1, -1, -1, -1, -1};
        String[] posAndTime = text.split("for");
        String[] pos = posAndTime[0].trim().split("to");
        int[] from = getPosition(pos[0].trim());
        int[] to   = getPosition(pos[1].trim()); // TODO index out bound
        int time   = posAndTime.length < 2 ? -1 : getTime(posAndTime[1].trim());
        return new int[]{ from[0], from[1], to[0], to[1], time };
    }

    int[] processHoldCommand(String text){ // TODO lift command
        if(text.equals("cancel")) return new int[]{1, -1, -1};
        String[] posAndTime = text.split("for");
        int[] pos = getPosition(posAndTime[0].trim());
        int time   = posAndTime.length < 2 ? -1 : getTime(posAndTime[1].trim());
        return new int[]{ pos[0], pos[1], time };
    }

    void toggleScreenOn(boolean wantScreenOn){
        if(wantScreenOn){
            if(screenOnView.getParent() == null) windowManager.addView(screenOnView,screenOnParams);
        } else {
            if(screenOnView.getParent() != null) windowManager.removeView(screenOnView);
        }
    }

    public static int getStatusBarHeight(Context context) {
        int statusBarHeight = 0;
        Resources res = context.getResources();
        int resourceId = res.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = res.getDimensionPixelSize(resourceId);
        }
        return statusBarHeight;
    }

    @Override
    public void onError(Exception error) {
        /*((TextView) findViewById(R.id.caption_text)).setText(error.getMessage());*/
    }

    @Override
    public void onTimeout() {
        switchSearch(KWS_SEARCH);
        if (glassView.getParent() != null) windowManager.removeView(glassView);
    }

    void touch(float fromX, float fromY, float toX, float toY, long duration){
        GestureDescription.Builder builder = new GestureDescription.Builder();
        Path path = new Path();
        path.moveTo(fromX, fromY);
        path.lineTo(toX, toY);
        builder.addStroke(new GestureDescription.StrokeDescription(path, 0, duration, false));
        dispatch(builder.build());
    }

    boolean isDispatching = false;
    int dispatchTrial = 0;
    int MAX_TRIAL_COUNT = 20;

    public void dispatch(final GestureDescription g) {
        isDispatching = true;
        dispatchGesture(g, new GestureResultCallback() {
            public void onCompleted(GestureDescription gestureDescription) {
                dispatchTrial = 0;
                isDispatching = false;
                Log.d("z", "dispatch");
            }

            public void onCancelled(GestureDescription gestureDescription) {
                if(dispatchTrial++ < MAX_TRIAL_COUNT) {
                    dispatch(g);
                }
            }
        }, null);
    }

    void screenOn() {
        PowerManager.WakeLock wl = powerManager.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "app:CHESS");
        wl.acquire(10*60*1000L);
        wl.release();
    }
}
